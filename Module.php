<?php declare(strict_types = 1);
 
namespace Modules\TopologyView;

use APP;
use CController as CAction;

class Module extends \Zabbix\Core\CModule {
	private $config = [];
	/**
	 * Initialize module.
	 */
	public function init(): void {
		
		$this->loadConfig();
		// Initialize main menu (CMenu class instance).
		$roots_menu = new \CMenu();
		$problems_menu = new \CMenu();

		foreach ($this->config['topology_tags'] as $topo_tag => $topo_descr) {
			$roots_menu->addItem( (new \CMenuItem(_($topo_descr)))
										->setUrl((new \CUrl('zabbix.php'))
										->setArgument('action', 'topology.rootslist')
										->setArgument('tag', $topo_tag)
										, ''));
			
			
			//NOTE: topology analysis is done on buidling topology, so far this is removed
			//however it might be feasible to allow run it from the UI to a) regen b) to see anomalies in UI, not in the console
			// $problems_menu->addItem( (new \CMenuItem(_($topo_descr)))
			// 							->setUrl((new \CUrl('zabbix.php'))
			// 							->setArgument('action', 'topology.analyze')
			// 							->setArgument('tag', $topo_tag)
			// 							, ''));
		};

		APP::Component()->get('menu.main')
			->findOrAdd(_('Monitoring'))
				->getSubmenu()
					->insertAfter('Hosts', 
				
					(new \CMenuItem(_('Topologies')))
						->setSubMenu(
							new \CMenu([
							(new \CMenuItem(_('Roots list')))->setSubMenu($roots_menu),
						//	(new \CMenuItem(_('Topology problems')))->setSubMenu($problems_menu)
						])
					)
		);

		$links_helper = function($a,$b,$c) {
			return $this->links_helper_func($a,$b, $c);
		};

		$linksHelper = APP::Component()->get('links.context.handler');
		$linksHelper->addLinksHandler('host', 'short', '100', $links_helper);
	}
 
	private function loadConfig() {
		$sfile = dirname(__FILE__).'/settings.php';
	
	    if (file_exists($sfile)) {
			$this->config = include($sfile);
 		} else {
			$this->config = ['topology_tags' => [ 'topology_lldp' => 'Access topology',] ];
		}
	}
	
	private function links_helper_func($data, $type, $context) {
		if ('short' == $context && 'host' == $type && isset($data['tags'])) {
			$links = [];

			foreach ($data['tags'] as $tag) 
				if ($this->isTopologyTag($tag)) 
					$links[] = [$this->createTopologyLink($data, $tag)," "];
		
			foreach ($data['inheritedTags'] as $tag) 
					if ($this->isTopologyTag($tag)) 
						$links[] = [$this->createTopologyLink($data, $tag)," "];
				

			return $links;
		}
		return [];
	}

    private function createTopologyLink($data, $tag) {
		$hostid = $data['hostid'];
		return (new \CLink(
				$this->config['topology_tags'][$tag['tag']],
				(new \CUrl('zabbix.php'))
					->setArgument('action','topology.view')
					->setArgument('hostid', $hostid)
					->setArgument('tag',$tag['tag'])
					->setArgument('onlyrootpath','1')
				));
	}
	
	private function isTopologyTag($tag) {
		
		if (isset($this->config['topology_tags'][$tag['tag']]))
			return true;

		return false;
	}
}

?>
