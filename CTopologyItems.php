<?php declare(strict_types = 1);
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Modules\TopologyView;

define('TAG_LINK_USAGE',    'link_usage');
define('TAG_LINK_INFO', 'link_info');
define('TAG_LINK_LEGEND', 'link_legend_');
define('TAG_HOST_INFO', 'host_info');

define('TAG_CLASS',    'class_');
define('TAG_SEARCH',    'search');
define('TAG_WEIGHT',    'weight');
define('TAG_NEIGHBOR',  'neighbor');
define('TAG_SYSNAME',   'sysname');


require_once 'CTopologyUtils.php';


class CTopologyItems {
    private $items = [];
    private $item_severitites = [];
    private $itemid_to_entity = [];
    private $items_by_hostid = [];
    
    private $entity_data = [];
    private $max_entity_data = 0;

    private $entites = [];
    private $hosts;
    private $logger;

    public function __construct(&$logger, CTopologyHosts $hosts, $topology_tag) {
        $this->logger = $logger;
        $this->hosts = $hosts;
        $this->topology_tag = $topology_tag;
    }

    public function isSysnameItem(array &$item) {
        if (!isset($item['lastvalue']) || is_numeric($item['lastvalue']) || strlen($item['lastvalue']) < 1) 
            return false;
      
        return $this->itemHasTag($item, $this->topology_tag, TAG_SYSNAME);
    }

    private function itemHasTag(array &$item, $tagname, $value) {
        foreach ($item["tags"] as $tag) {
            if ($tag['tag'] == $tagname && $tag['value'] == $value )  
                return true;
        }
        
        return false;
    }

    private function itemHasTagsStarts(array &$item, $tagname, &$tag_suffix) {
        $tag_len = strlen($tagname);

        if (0 == $tag_len)
            return false;

        foreach ($item["tags"] as $tag) {
                           
            if ($tag['tag'] = $this->topology_tag && substr($tag['value'],0,$tag_len) == $tagname)  {

                $tag_suffix = substr($tag['value'],$tag_len);
                return true;
            }
        }
        return false;
    }

    public function isNeighborItem(array &$item) {
        if (isset($item['lastvalue']) && 
               (is_numeric($item['lastvalue']) || strlen($item['lastvalue']) > 0 )) {
            return $this->itemHasTag($item, $this->topology_tag, TAG_NEIGHBOR);
        }
        
        return false;
    }

    public function isWeightItem(array &$item) {
        if (isset($item['lastvalue']) && 
               (is_numeric($item['lastvalue']) || strlen($item['lastvalue']) > 0 )) {
            return $this->itemHasTag($item, $this->topology_tag, TAG_WEIGHT);
        }
        
        return false;
    }

    public function isDataItem(array &$item) {
        if (isset($item['lastvalue']) && 
               (is_numeric($item['lastvalue']) || strlen($item['lastvalue']) > 0 )) {
            
            return ($this->itemHasTag($item, $this->topology_tag, TAG_LINK_USAGE ));
        }
        
        return false;
    }


    public function isClassItem(array &$item, &$class_name) {
        if (isset($item['lastvalue']) && 
               (is_numeric($item['lastvalue']) || strlen($item['lastvalue']) > 0 )) 

            return $this->itemHasTagsStarts($item, TAG_CLASS, $class_name);

        return false;
    }

    public function isLinkInfoItem(array &$item) {
        if (isset($item['lastvalue']) && 
               (is_numeric($item['lastvalue']) || strlen($item['lastvalue']) > 0 )) 

            return $this->itemHasTag($item, $this->topology_tag, TAG_LINK_INFO);

        return false;
    }
    
    private function isHostInfoItem(array &$item) {
        if (isset($item['lastvalue']) && 
               (is_numeric($item['lastvalue']) || strlen($item['lastvalue']) > 0 )) 

            return $this->itemHasTag($item, $this->topology_tag, TAG_HOST_INFO);

        return false;
    }

    private function isSearchItem(array &$item) {
        if (isset($item['lastvalue']) && 
               (is_numeric($item['lastvalue']) || strlen($item['lastvalue']) > 0 )) 

            return $this->itemHasTag($item, $this->topology_tag, TAG_SEARCH);

        return false;
    }


    public function isLinkLegendItem( array &$item, &$legend_name) {
        if (isset($item['lastvalue']) && 
               (is_numeric($item['lastvalue']) || strlen($item['lastvalue']) > 0 )) 

            return $this->itemHasTagsStarts($item, TAG_LINK_LEGEND, $legend_name);

        return false;
    }


    public function saveItems(array &$items) {
        foreach ($items as $item)  {
            $this->items_by_hostid[$item['hostid']][] = $item['itemid'];
            $this->items[$item['itemid']] = $item;
   
            if ($this->isSysnameItem($item)) {
                $this->hosts->saveSysname($item['lastvalue'], $item['hostid']);
                continue;
            } 

            if ($this->isWeightItem($item)) 
                $this->hosts->setMaxWeightHost($item['hostid'], $item['lastvalue']);
            

            if ($this->isDataItem($item)) {
                $entity = $this->getItemEntity($item['itemid']);
                $this->addEntityData($item['hostid'], $entity, $item['lastvalue']);
            }
        }
    }
                     
    public function getItemsByHostId($hostid) {
        $ret = [];
        
        if (!isset($this->items_by_hostid[$hostid]))
            return [];
        
        foreach ($this->items_by_hostid[$hostid] as $itemid) 
            $ret[] = $this->items[$itemid];
        
        return $ret;
    }

    public function &getItemById($itemid) {
        $item = NULL;
        if (isset($this->items[$itemid]))
             return $this->items[$itemid];
        
        return $item;
    }
    
    public function &getItemsByIds($itemids) {
        $items = [];
        
        foreach ($itemids as $itemid) {
            
            if (isset($this->items[$itemid]))
                 $items[]=$this->items[$itemid];
        }
        
        return $items;
    }

    public function dump() {
        return \json_encode($this->items);
    }

    public function &get() {
        return $this->items;
    }

    public function getItemEntity($itemid) {
        if (isset($this->itemid_to_entity[$itemid]))
            return $this->itemid_to_entity[$itemid];
        return NULL;
    }

    public function getEntityItems($hostid, $entity_name) {
        if (isset($this->entity_to_itemids[$hostid][$entity_name]))
            return $this->entity_to_itemids[$hostid][$entity_name];
        return [];
    }

    public function getFormattedValue(array &$item) {
        return formatHistoryValue($item['lastvalue'], $item, false);
    }

    public function getValuesByTopoTag(array $items) {
        $results = [];

        foreach ($items as $itemid) {
            $item = $this->getItemById($itemid);
                
            if (! isset($item['tags']) || !\is_array($item['tags']))
                continue;
                
            foreach ($item['tags'] as $tag) {
                if ($tag['tag'] == $this->topology_tag) 
                    $results[$tag['value']] = $item['lastvalue'];
                    $results[$tag['value'].'_formatted'] = formatHistoryValue($item['lastvalue'], $item, false);
            }
        }
       
        return $results;
    }

    static public function fetchItemsAndEntities(array $hostids, array $req_itemids, array $tag_filter) {
     
        $api_items_request = [
            'output' => ['itemid','lastvalue','hostid','key_','units','value_type','name'],
            'selectValueMap' => 'extend',
            'selectTags' => 'extend',
            'selectInheritedTags' => 'extend',
            'selectTemplates' => ['templateid','name'],
            'selectDiscoveryRule' => ['itemid', 'name', 'templateid', 'key_'],
            'selectItemDiscovery' => ['parent_itemid','itemdiscoveryid','itemid'],
            'nodebug' => 1
        ];

        if (count($hostids) > 0 ) 
            $api_items_request['hostids'] = $hostids;
    
        if (count($req_itemids) > 0 ) 
           $api_items_request['itemids'] = $req_itemids;
    
        if (count($tag_filter) > 0) 
            $api_items_request['tags'] = $tag_filter;
    

        $items =\API::Item()->get($api_items_request);
       
        $itemids = array_column($items, 'itemid');
        
        if (count($hostids) ==0 ) 
            $hostids = array_column($items, 'hostid');
                
        $entities = \API::DiscoveryEntity()->get([
            'hostids' => $hostids,
            'items' => $items,
            'nodebug' => 1
        ]);
      
        return [$entities, $items];
    }

    public function fetchAndSaveItems(array $hostids, array $req_itemids, array $tag_filter) {
     
        [$entities, $items] = self::fetchItemsAndEntities($hostids, $req_itemids, $tag_filter);

        $this->saveEntities($entities);
        $this->saveItems($items);
    }

    public function saveEntities(array &$entities) {
        
        foreach ($entities as $entityid => $entity ) {
            if (!isset($entity['entities']) || !is_array($entity['entities']))
                continue;

            foreach ($entity['entities'] as $ent_name => $ent_items) {
                foreach ($ent_items as $proto_itemid => $itemid) {
                   // $this->logger->log("Saving entity for itemid ".\json_encode($itemid)." entity $ent_name");
                    $this->itemid_to_entity[$itemid] = $ent_name;
                    $this->entity_to_itemids[$entity['hostid']][$ent_name][]=$itemid;
                }
            }
        }
    }

    public function addEntityData($hostid, $entity, $data) {
        if (!isset($this->entity_data[$hostid]))
            $this->entity_data[$hostid] = [];
        
        if (!isset($this->entity_data[$hostid][$entity]))
        $this->entity_data[$hostid][$entity] = 0;
            
        $this->entity_data[$hostid][$entity] += $data;
        $this->max_entity_data = max($this->entity_data[$hostid][$entity], $this->max_entity_data);
    }

    public function getEntityData($hostid, $entity) {
        if (!isset($this->entity_data[$hostid]) || 
            !isset($this->entity_data[$hostid][$entity]))
        return 0;
        
        return $this->entity_data[$hostid][$entity];
    }

    public function getMaxEntityData() {
        return $this->max_entity_data;
    }

    public function dumpEntityData() {
        return \json_encode($this->entity_data);
    }

    private function addHostItemData($item) {
        return $item['name'];
    }
    
    public function makeHostItemsData($hostid) {
        $host_items_data ='';

        $items = $this->GetItemsByHostId($hostid);
        
        foreach($items as $item) {
            if ($this->isHostInfoItem($item) ) {
                $host_items_data.= '<b>'.$item['name'].'</b> : '.CTopologyUtils::escape_json($this->getFormattedValue($item)).'</br>';
            }

        }
        return '"'.$host_items_data.'"';

    }

    public function getItemsSearchText(array $items, $init_text) {
        
        $search_text = $init_text;

        foreach ($items as $item) {
            
            if ($this->isSearchItem($item)) 
                $search_text.=" ".$item['lastvalue'];     
        }

        return $search_text;
    }
}