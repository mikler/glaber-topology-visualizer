<?php declare(strict_types = 1);
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Modules\TopologyView;


class CTopologyDeps {
   
   private array $deps_up = [];
   private array $deps_down = [];
   private array $topo_hostids = []; //all hostids hash forming the current topology
   private $up_down_fetch_func;
   private $db_deps = [];

   public function __construct($topology_tag) {
     
      $db_deps = \API::HostDepends()->get(['filter' => ["name" => $topology_tag] ]);

  //    \show_error_message("Loaded ". count($db_deps)." host depends from the API");
  //    \show_error_message("Deps are ". json_encode($db_deps)." host depends from the API");

      $this->db_deps = $db_deps;

      foreach ($db_deps as $idx=>$dep) 
         $this->saveTopoDep($dep);
   }
   
   private function saveTopoDep(array &$dep) {
      if (!isset($this->deps_up[$dep['hostid_down']]))
         $this->deps_up[$dep['hostid_down']]=[];
      
      if (!isset($this->deps_down[$dep['hostid_up']]))
         $this->deps_down[$dep['hostid_up']]=[];
      
      $this->deps_up[$dep['hostid_down']][] = $dep['hostid_up'];
      $this->deps_down[$dep['hostid_up']][] = $dep['hostid_down'];
   }
   
   private function get_up_down_hosts($hostid) {
      $hostids = [];
      if (isset($this->deps_down[$hostid]))
         $hostids = array_merge($hostids, $this->deps_down[$hostid]);

      if (isset($this->deps_up[$hostid]))
         $hostids = array_merge($hostids, $this->deps_up[$hostid]);

      return $hostids;
   }

   private function get_up_hosts($hostid) {
      $hostids = [];

      if (isset($this->deps_up[$hostid]))
         $hostids = array_merge($hostids, $this->deps_up[$hostid]);

      return $hostids;
   }

//this function will fill topo_hostids - only ids of the hosts that are linked to the topology
   private function loadTopoHosts($hostid, $depth, $neighbor_function) {
   
      if (isset($this->topo_hostids[$hostid]) || 0 == ($depth--) )
         return;
      
      $this->topo_hostids[$hostid]=1;
      
      $neighbor_hosts = call_user_func(array($this,$neighbor_function), $hostid);
   
      if (count($neighbor_hosts) < 1)
          return;

      foreach ($neighbor_hosts as $idx => $neighbor_host)  
         $this->loadTopoHosts($neighbor_host, $depth, $neighbor_function);

  }

   public function loadTopologyFull($hostid, $depth = 10) {   //fetches all topo neighbors related to the host 
      $this->loadTopoHosts($hostid, $depth, 'get_up_down_hosts');
   }

   public function loadTopologyUp($hostid, $depth = 5) {   //fetches all topo neighbors related to the host 
      $this->loadTopoHosts($hostid, $depth, 'get_up_hosts');
   }

   public function getTopoNeighbors($look_hostid) {
      $hostids = [];
      //$all_neighbors = array_merge($this->deps_down[$look_hostid],$this->deps_up[$look_hostid]);
      
      //\show_error_message("All host $look_hostid neighbors are ".json_encode($all_neighbors));
      
      // foreach ( $all_neighbors as $ids => $hostid) {
      //    \show_error_message($hostid);
      //    if (isset($this->topo_hostids[$hostid]))
      //       $hostids[] = $hostid;   
      // }
      
      // \show_error_message("Goting  all host $look_hostid topo neigbors: ".json_encode($hostids));
       return $this->get_up_down_hosts($look_hostid);
   }

    public function getTopoNeighborsUp($hostid) {
       return $this->get_up_hosts($hostid);
    }

   public function getTopoHostsids() {
      return $this->topo_hostids;
   }

   public function DumpAllDeps() {
      return [
         'deps_up' => $this->deps_up,
         'deps_down' => $this->deps_down
      ];
   }

   public function get_deps_db() {
      return $this->db_deps;
   }
}