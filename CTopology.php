<?php 
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/


declare(strict_types = 1);
namespace Modules\TopologyView;

error_reporting(-1);
ini_set('display_errors', 'On');

define("MAX_TOPO_RADIUS", 16);

define("ALLOW_SUBSTRING_HOST_SEARCH", false);
define("MAX_WIDTH", 30);
define("MIN_WIDTH", 1);
define("DEFAULT_WIDTH", 2);

define("TOPOLOGY_TREE", 1);
define("TOPOLOGY_EQUAL", 2);

define('TOPOLOGY_TYPE_FULL', 1);
define('TOPOLOGY_TYPE_ONLY_DEPS', 2);
define('TOPOLOGY_TYPE_ROOTTREE', 3);

require_once 'CTopologyLogger.php';
require_once 'CTopologyEdges.php';
require_once 'CTopologyHosts.php';
require_once 'CTopologyEntities.php';
require_once 'CTopologyDeps.php';

class CTopology{
    
    private $edges;
    private $logger;
    private $hosts;
    private $items;

    private $topology_tag;
    private $topology_type = TOPOLOGY_TREE;
    private $map_hosts = [];
   
   
    private $last_angle = 0;

    function __construct($hostid, $topology_tag, $onlyrootpath, &$nodes, &$links, &$hostname,  $options) {
               
        $this->logger = new CTopologyLogger();

        //theese are mandatory, and the map should be build only having 
        //hosts and l
        $this->hosts = new CTopologyHosts($this->logger, $topology_tag);
        $this->deps = new CTopologyDeps($topology_tag);
        
        //all unnecessary parts 
        if (isset($options['load_metrics'])) {
            $this->items = new CTopologyItems ($this->logger, $this->hosts, $topology_tag);
            $this->entities = new CTopologyEntities;
            $this->edges = new CTopologyEdges($this->logger);
            $this->hosts->setItems($this->items);
        }
        
        
        $this->topology_tag = $topology_tag;
        $this->logger->log("Calculating topology for host ". $hostid." topology tag ".$this->topology_tag );
        
        $start = floor(microtime(true) * 1000);
        $all_start = floor(microtime(true) * 1000);
        
        $host=\API::Host()->get([
              'hostids' => $hostid,
              "output" => ['hostid','host'],
              'filter' => [
                  'status' => [HOST_STATUS_MONITORED, HOST_STATUS_NOT_MONITORED]
              ]])[0];
     
        $hostname = $host['host'];
        
//        \show_error_message("Host fetch from api took " .number_format(floor(microtime(true) * 1000) - $start) ." s");
        
   //     $start = floor(microtime(true) * 1000);

 //       if (isset($onlyrootpath)) {
           // show_error_message("Loading only host->root tree for host $hostid");
   //         $this->deps->loadTopologyUp($hostid);
     //   }
       //  else
        $this->deps->loadTopologyFull($hostid);
        
    //   \show_error_message("Dependency load took " .number_format(floor(microtime(true) * 1000) - $start) ." s");
   //    \show_error_message("Resulting dependancy is  " . \json_encode( $this->deps->DumpAllDeps()));

        

      // $start = floor(microtime(true) * 1000);
      $this->hosts->fetchHostsDataFromAPI(array_keys($this->deps->getTopoHostsids()));
     // \show_error_message("fetch topology data from api " .number_format(floor(microtime(true) * 1000) - $start) ." s");

        //$start = floor(microtime(true) * 1000);
        //$starthostid = $this->hosts->getMaxWeightHostid();
       // $starthostid = $this->deps->getTopMostHostid();
       
       //$this->CalcTopology($hostid, MAX_TOPO_RADIUS, TOPOLOGY_TYPE_FULL);
        //there is no real reason to actually CALC the topology, it's already calculated
        //we just need to enumerate hosts having links to the hostid
        
      //  $this->deps->loadTopologyFull($hoistid, 10);
        
      $this->map_hosts = $this->deps->getTopoHostsids();
        // show_error_message("Get ". count($this->map_hosts)." hosts belonging to the topology".
        //          \json_encode($this->deps->getTopoHostsids()));

       
       // if (1 > $starthostid) {
       //     $starthostid = $hostid;
       //     $this->topology_type = TOPOLOGY_EQUAL;
       // }
      //  \show_error_message("Time used so far " .(floor(microtime(true) * 1000) - $all_start) ." ms");

        //$this->logger->log("Selected host $starthostid instead of $hostid for starting the topology");
        
        //$start = floor(microtime(true) * 1000);
        foreach($this->map_hosts as $ids=>$hostid)
            $this->AddMapHost($hostid, MAX_TOPO_RADIUS);
        
       // \show_error_message("Time used so far " .(floor(microtime(true) * 1000) - $all_start) ." ms");
        //\show_error_message("AddMapHost" .number_format(floor(microtime(true) * 1000) - $start) ." s");

        //$start = floor(microtime(true) * 1000);
        // if (isset($onlyrootpath))
        //     $this->CalcTopology($starthostid, MAX_TOPO_RADIUS, TOPOLOGY_TYPE_ROOTTREE);
        // else 
        //     $this->CalcTopology($starthostid, MAX_TOPO_RADIUS, TOPOLOGY_TYPE_FULL);
        //\show_error_message("Calc topology took " .number_format(floor(microtime(true) * 1000) - $start) ." s");
        
       // \show_error_message("Calculated edges are". $this->edges->dumpLinksData());
       // \show_error_message("Calculated links are". json_encode($this->links));

        //$start = floor(microtime(true) * 1000);
       // $this->edges->mergeLinksData();
        //\show_error_message("merge Links Data " .number_format(floor(microtime(true) * 1000) - $start) ." s");
        
        //$start = floor(microtime(true) * 1000);
        if (isset($options['load_metrics']))
            $this->generateClasses();
        //\show_error_message("Generate classes " .number_format(floor(microtime(true) * 1000) - $start) ." s");

      //  $start = floor(microtime(true) * 1000);
        
        $this->SaveMapProblems($options);
       // \show_error_message("Save map problems " .number_format(floor(microtime(true) * 1000) - $start) ." s");
        
      //  $this->logger->output_log();
      //  \show_error_message('Topology is '.json_encode($this->deps->getTopoHosts()));
      //\show_error_message("Total server calc time " .number_format(floor(microtime(true) * 1000) - $all_start) ."ms");
    }

    public function getTopologyType() {
        return $this->topology_type;
    }
    
    private function SaveMapProblems($options) {

        $hostids = array_keys($this->map_hosts);
        
        //\\show_error_message("Fetching trigger states for ".count($hostids). " hosts");

        // $triggers = \API::Trigger()->get([
        //     "selectTags" => "extend",
        //     "output" => "extend",
        //     "hostids" => $hostids,
        //     "recent" => "true",    
        //     "selectItems" => "true", 
        //     "selectHosts" => "true", 
        //     'nodebug' => 1,
        //     "filter" => [
        //         "value" => 1,
        //     ],
        // ]);
       //\show_error_message("Api: trigger get " .number_format(floor(microtime(true) * 1000) - $start) ." s");

       $missed_items = [];
        if (isset($options['load_metrics'])) {
            foreach ($triggers as $trigger) 
                foreach ($trigger['items'] as $item) {
                    $entity = $this->items->getItemEntity($item['itemid']);
                
                    if (NULL == $entity) {
                            $missed_items[] = $item['itemid'];
                    }
                }

            if ( count($missed_items) > 0 ) 
                    $this->items->fetchAndSaveItems([], $missed_items, []);
        }


        $states = \CZabbixServer::getHostsProblemsCounters(\CSessionHelper::getId(),  $hostids);

		foreach ($states as $host_state) {
			$hosts[$host_state['hostid']]['problem_count']=$host_state['severities'];

            foreach ($host_state['severities'] as $idx=>$has_severity) {
                if ($has_severity) 
                    $this->hosts->setHostSeverity($host_state['hostid'], $idx); 
            }
        }
    }
  
    // private function CalcTopology($hostid, $depth, $topo_type) {
    //     if ($depth < 1)
    //         return;

    //     $neighbor_hosts = $this->deps->getTopoNeighbors($hostid);
    //     \show_error_message("Got neighbor hosts for host $hostid". json_encode($neighbor_hosts));

    //     $this->logger->log("Calculating topo for $hostid : got neighbor hosts: ". \json_encode($neighbor_hosts));

    //     foreach ($neighbor_hosts as $neighbor_host_id) {
            
    //   //      $itemids =[];
    //   //      $itemids = $this->hosts->getHostsInterLinks($hostid, $neighbor_host_id);
            

    //         if ($this->MapHostExists($neighbor_host_id)) {

    //     //        if ($this->edges->hasBackLink($hostid, $neighbor_host_id))
    //       //          continue;

    //      //       $this->edges->saveLinks($hostid, $neighbor_host_id, $itemids['from']);
    //      //       $this->edges->saveBackLinks($neighbor_host_id, $hostid, $itemids['to']);          
                
    //             continue;  
    //         }

    //         $this->AddMapHost($neighbor_host_id, $depth);
    //         $this->logger->log("Got interlinks for neighbor $hostid -> $neighbor_host_id". \json_encode($itemids));

    //        // $this->edges->saveLinks($hostid, $neighbor_host_id, $itemids['from']);
    //         //$this->edges->saveBackLinks($neighbor_host_id, $hostid, $itemids['to']);    
            
    //         $this->CalcTopology($neighbor_host_id, $depth-1, $topo_type);
    //     }
    // }

    private function MapHostExists($hostid) {
        return isset($this->map_hosts[$hostid]);
    }

   
    public function GetAntvG6NodesMap(){
        $output ='[';
        $i = 0;
        
  //      \show_error_message(json_encode($this->map_hosts));
  //      \show_error_message(json_encode($this->hosts));

        foreach ($this->map_hosts as $hostid => $host_map_data) {
            $host = $this->hosts->getById($hostid);
//            \show_error_message("Got host".json_encode($host));

            if (!isset($host['hostid']))
                continue;
       
            if ($i++ > 0) 
                $output.=",\n";
            
       
            $color = '';

            if ($this->hosts->getHostSeverity($hostid) > 0) {
                $color = '"stroke": "#'.\CSeverityHelper::getColor($this->hosts->getHostSeverity($hostid), 1).'",';
            }
   
            $isUnknown = 'false';
            if ($this->hosts->isUnknownHostId($hostid)) {
                $color = '"stroke": "#AAA",';
                $isUnknown = 'true';
            }

            $label = CTopologyUtils::escape_json($host['host']);
            $search_text = $label;
            
            $hostitems = NULL;
            //$hostitems = $this->items->getItemsByHostId($host['hostid']);
            
           // if (is_array($hostitems))
           //     $search_text = $this->items->getItemsSearchText($hostitems, $label);
           //"itemsdata" : '. $this->items->makeHostItemsData($hostid).', 

           //"x":'. $host_map_data['x'].',
           //"y":'. $host_map_data['y'].' 
            $output .= '{ "id": "'. $host['hostid'].'",
                          "label":"'.CTopologyUtils::escape_json($host['host']).'",
                          "search":"'.$search_text.'",
                          "style": {'.$color.'},
                          "unknown" : "'.$isUnknown.'",
               
                        }';
       }

       $output .= ']';
       return $output;
    }
 
    // public function GetAntvG6EdgesMap(){
   
    //     //$links = $this->deps->getLinksData();
        
    //     if (!isset($links))
    //         return '[]';
        
    //     $output ='[';
    //     $i = 0;
        
    //     foreach ($links as $from_host_id => $hostlinks) {
    //         foreach ($hostlinks as $to_host_id => $items) {
 
    //             $edge = new CTopologyEdge($from_host_id, $to_host_id, $items, $this->items, $this->entities, $this->hosts);
                
    //             if ($edge->getItemsCount() == 0 ) {
    //               //  error_log("Warning! edge $from_host_id -> $to_host_id has no items at all\n");
    //                 continue;
    //             }

    //             if ($i++ > 0) 
    //                 $output.=",\n";
                
    //             $output .= $edge->getEdgesJSdata();
    //         }
     
    //     }
    //     $output .= ']';
    //     return $output;
    // }
                
    public function GetAntvG6Selectors($parent_obj) {

        $hosts_class_names = $this->hosts->getClassesNames();
        if (count($hosts_class_names) == 0)
            return '';

        $js =' const filter_caption = document.createTextNode("  Filter:");
        '.$parent_obj.'.appendChild(filter_caption);';

        $i = 1;
        

        foreach ($hosts_class_names as $classname) {

            $hosts_classes = $this->hosts->getClasses($classname);

            if (count($hosts_classes) == 0)
                continue;

            $j = 1;
            $js .='      
                const selector'.$i.' = document.createElement("select");
                selector'.$i.'.id = "selector";
       
                const filter_label'.$i.' = document.createTextNode(" '.$classname.' ");
                '.$parent_obj.'.appendChild(filter_label'.$i.');


                const selection'.$i.'_0 = document.createElement("option");
                selection'.$i.'_0.value = "default";
                selection'.$i.'_0.innerHTML = "Not selected";
                selector'.$i.'.appendChild(selection'.$i.'_0);
            ';
           // sort($hosts_classes);
            sort($hosts_classes,SORT_NUMERIC);
            foreach ($hosts_classes as $host_class ) {

                $host_class = CTopologyUtils::escape_json($host_class);
                $js .= '
                const selection'.$i.'_'.$j.' = document.createElement("option");
                selection'.$i.'_'.$j.'.value = "'.$host_class.'";
                selection'.$i.'_'.$j.'.innerHTML = "'.$host_class.'";
                selector'.$i.'.appendChild(selection'.$i.'_'.$j.');
                ';
                $j++;
            }

            $js.=$parent_obj.'.appendChild(selector'.$i.');
        
            nodeclasses'.$i.' = '.$this->hosts->dumpHostClasses($classname).';
            const classname'.$classname.' = "'.$classname.'";

            selector'.$i.'.addEventListener("change", (e) => {
                const class_val = e.target.value;
                console.log("Selected class "+ class_val);
            //    console.log("Hosts: " + classes'.$i.'[e.target.value]);
            
                graph.getNodes().forEach(node => {
                    graph.setItemState(node, "active", false);
                });

                graph.getEdges().forEach(edge => {
                    graph.setItemState(edge, "active", false);
                });
                

                nodeclasses'.$i.'[class_val].forEach(id =>  {
                    node = graph.findById(id);
                    graph.setItemState(node, "active", true);
                });
            
                graph.getEdges().forEach(edge => {
                    if ( typeof(edge._cfg.model.classes[classname'.$classname.']) == "object" && 
                            edge._cfg.model.classes[classname'.$classname.'].includes(class_val) ) {
                        graph.setItemState(edge, "active", true);
                    }
                });
            
            });
            ';
            $i++;
        }
        return $js;
    }

 
  
    private function AddMapHost($hostid, $depth) {
       
        $radius =( MAX_TOPO_RADIUS - $depth + 1 ) * 5;

        if (isset($this->map_hosts[$hostid]) && isset($this->map_hosts[$hostid]['hostid']))
            return false;

        if (!isset($this->map_hosts[$hostid]))
            $this->map_hosts[$hostid] = ['id' => count($this->map_hosts) + 1,
                                        'x' => 400 + $radius * cos($this->last_angle * pi() / 180),
                                        'y' => 400 + $radius * sin($this->last_angle * pi() / 180),
                            ];
     
        $this->last_angle++;                    
        return true;
    }

    private function generateClasses() {
        foreach ($this->items->get() as $item) {
            $classname = '';
            if ($this->items->isClassItem($item, $classname) &&
                isset($this->map_hosts[$item['hostid']])
            
            ) 
            {
               // $this->logger->log("Adding host class '$classname' from value ".$item['lastvalue']. " to host ". $item['hostid']);
                $classes = [];
                if (is_string($item['lastvalue']))
                    $classes = explode(',',$item['lastvalue']);
                else 
                    $classes = [$item['lastvalue']];

                $this->hosts->setHostClasses($item['hostid'],$classname, $classes);
   
            }
        }
    }
    
    public function GetAntvG6EdgesMap(){
        $ret = "";
        $i=0;
        foreach($this->deps->get_deps_db() as $id => $dep ) {
            
            if (isset($this->map_hosts[$dep['hostid_up']])) {
                if ($i++ > 0)
                    $ret .=',';
                       
                $ret .= '{"source":"'. $dep['hostid_up']. '", "target":"'. $dep['hostid_down'].'"}';
            }
                     
        }
        return '['.$ret.']';
    }
}