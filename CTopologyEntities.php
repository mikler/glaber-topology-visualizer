<?php declare(strict_types = 1);
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Modules\TopologyView;

class CTopologyEntities {
    private $entity_severities = [];
    private $entity_triggers = [];

    private $max_data = 0;

    public function setSeverity($hostid, $entity_name, $severity) {
        if (!isset($this->entity_severities[$hostid][$entity_name]) ||
                $this->entity_severities[$hostid][$entity_name] < $severity ) 
        {
            $this->entity_severities[$hostid][$entity_name] = $severity;
        }
    }
    
    public function getSeverity($hostid, $enity_name) {
        if (!isset($this->entity_severities[$hostid]))
            return 0;
        if (!isset($this->entity_severities[$hostid][$enity_name]))
            return 0;
        return  $this->entity_severities[$hostid][$enity_name];
     
    }
    
    public function addTrigger($hostid, $entity_name, $triggerid) {
        $this->entity_triggers[$hostid][$entity_name][] = $triggerid;
    }

    public function getTriggers($hostid, $entity_name) {
        if (isset($this->entity_triggers[$hostid][$entity_name]))
            return $this->entity_triggers[$hostid][$entity_name];
        
        return [];
    }

    public function dump() {
        return \json_encode([ "severities"=> $this->entity_severities, 
                "triggers" => $this->entity_triggers ]); 
    }

}
