#!/usr/bin/env php
<?php 
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

//builds host's dependancy based on the topology data used in
//the format of glaber-topology-visualizer module
//uses zabbix api implementation from https://github.com/intellitrend/zabbixapi-php

declare(strict_types = 1);

require_once(__DIR__ . "/CHostDependsBuilder.php");

$sfile = dirname(__FILE__).'/../settings.php';
$config = include($sfile);

foreach ($config['topology_tags'] as $topo_tag => $topo_descr ) {
    echo "Building topology: '$topo_descr', tag '$topo_tag'\n";
    
    $topo_builder = new CHostDependsBuilder($config['API_URL'], $config['API_KEY'], $topo_tag);
    $topo_builder->updateHostsDependencies();

}

