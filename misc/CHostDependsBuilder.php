<?php 
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

//builds host's dependancy based on the topology data used in
//the format of glaber-topology-visualizer module
//uses zabbix api implementation from https://github.com/intellitrend/zabbixapi-php

declare(strict_types = 1);

require_once(__DIR__."/ZabbixApi.php");

class CHostDependsBuilder {
    private $api;
    private $topo_name;
    private $sysnames;
    private $visited_hosts = [];
    private $topo = [];
    private $root_hosts = [];
    private $hostweights = [];
    private $no_neighbors = 0;
    private $no_roots = 0;
    private $deps = [];

    public function __construct($url, $token, $topo_name) {
        echo("Creating the connetion\n");
        $this->topo_name = $topo_name;

        $this->api = new IntelliTrend\Zabbix\ZabbixApi();
        $this->ApiConnectByToken($url, $token);
    }
    
    private function enumerateTopoHosts($hostid, array &$neighbor_hosts, &$group_info) {
        
        if (isset($this->visited_hosts[$hostid]))
            return;
        
        if (isset($this->root_hosts[$hostid])) {
            printf("Found root host $hostid \n");
            $group_info['root_hosts'][] = $hostid;
        }

        $neighbor_hosts[] = $hostid;

        $this->visited_hosts[$hostid] = 1;
        
        if (isset($this->hostweights[$hostid]) && $this->hostweights[$hostid] > $group_info['maxweight']) {
            $group_info['maxhost'] = $hostid;
            $group_info['maxweight'] = $this->hostweights[$hostid];
        }

        $neighbors = array_unique($this->topo[$hostid]);

        if (count($neighbors) < 1)
            return;

        foreach ($neighbors as $idx => $host) {
            $this->enumerateTopoHosts($host, $neighbor_hosts, $group_info);
        }
    }
    
    private function saveDep($host_up, $host_down) {
        $this->deps["$host_up -> $host_down"] = ['up' => $host_up, 'down' => $host_down];
    }

    //assuming hosid is the top-most host, creates hosts depends for hosts group
    private function saveHostsDeps($hostid, &$visited_hosts) {
       // printf("Saving hosts deps for host $hostid \n");
        
        $neighbors = array_unique($this->topo[$hostid]);
        //print("Got neighbors : ".json_encode($neighbors)." \n");

        if (count($neighbors) < 1)
            return;
        
        foreach ($neighbors as $idx => $host) {
  //          if (isset($this->root_hosts[$host]))
   //          continue;

            if (isset($visited_hosts[$host])) { //not returning where we've been
                continue;
            }

            $visited_hosts[$hostid] = 1;
            
            $this->saveDep($hostid, $host);
            $this->saveHostsDeps($host, $visited_hosts);
        }
    }

    private function BuildTopology() {
        $visited_hosts = [];

        foreach ($this->topo as $host => $neighbors) {
          
            if (0 == count($neighbors)) {
                $this->no_neighbors++;
                continue;
            }

            $neighbor_hosts = [];
            $group_info = ['root_hosts'=> [], 'maxhostid' => 0, 'maxweight' => 0];
          
            $this->enumerateTopoHosts($host, $neighbor_hosts, $group_info);
            
            $neighbor_hosts = array_unique($neighbor_hosts);
                        
            if (0 == count($neighbor_hosts)) 
                continue;

            printf("Got ".count($neighbor_hosts)." neighbors of host $host, total root hosts ".
                    count($group_info['root_hosts']). " max host id is ".$group_info['maxhostid']. 
                    " with weight ". $group_info['maxweight']."\n");
            
            //nominal case: has one root host
            if (0 < count($group_info['root_hosts']) ) {
                foreach ($group_info['root_hosts'] as $idx => $root) {
                    $visited_hosts =[];
                    $this->saveHostsDeps($root, $visited_hosts);
                }
            }

            if (0 == count($group_info['root_hosts']) ) {
                printf("There is no root host for the group, fetching weight info for ".count($neighbor_hosts)." hosts\n");
                $maxhostid = $this->ApiGetHostItemsWeights($neighbor_hosts);
                $this->no_roots += count($neighbor_hosts);

                printf("Calculated maxhostid by weight is $maxhostid \n");
                if (0 == $maxhostid) {
                    printf("Using first host as a root - weights are'nt defined or all are 0");
                    $maxhostid = $neighbor_hosts[0];
                }
                $visited_hosts = [];
                $this->saveHostsDeps($maxhostid, $visited_hosts);
            }
        }

        printf("There was ".$this->no_neighbors." hosts without neighbor info\n");   
        printf("Dumping resulting deps, total ".count($this->deps)."\n");
        
    }
    
    private function ApiGetHostItemsWeights(array &$hostids) {
        $maxweight = 0;
        $maxhostid = 0;

        $params = [
            'output' => ['itemid','lastvalue','hostid'],
            'evaltype' => 0,
            'hostids'=> $hostids,
            "selectTags" => ["tag", "value" ],
            'tags' => [['tag' =>$this->topo_name, 
                        'value' => 'weight', 'operator' =>1]],
            'nodebug' => 1
        ];
        
        $items =  self::ApiCall('item.get', $params);
        printf("Fetched ".count($items). " items\n");        
        
        foreach ($items as $id => $item) {
            if (!isset($this->hostweights[$item['hostid']]))
                $this->hostweights[$item['hostid']] = 0;
        
            $this->hostweights[$item['hostid']]+= $item['lastvalue'];
            if ($this->hostweights[$item['hostid']] > $maxweight) {
                $maxweight = $this->hostweights[$item['hostid']];
                $maxhostid = $item['hostid'];
            }
        }
        return $maxhostid;
    }

    public function updateHostsDependencies() {
        printf("Fetching topology info\n");
        [$topo, $conflicts, $unknown_hosts] = $this->getTopoFromAPI($this->topo_name);
        $this->topo = $topo;

        $this->buildTopology($topo);        
        
        printf("Saving topology\n");
        $this->ApiUpdateTopology();
    }
    
    private function getTopoFromAPI($topology_tag) {
        $sysnames = [];
        
        $topo = [];
        
        $conflicts = [];
        $unknown_hosts = [];
    
      //  printf("Fetching sysnames\n");
        $sysname_items = self::ApiGetTopoItemsByTag('sysname');
        
        //printf("Detecting conflicts\n");
        foreach ($sysname_items as $id => $item) {
            if ($item['lastvalue'] == "") 
                continue;
            
            if (isset($sysnames[$item['lastvalue']])) {
                $conflicts[$item['lastvalue']][] = $sysnames[$item['lastvalue']];
                $conflicts[$item['lastvalue']][] = $item['hostid'];
            }        
            $sysnames[$item['lastvalue']]=$item['hostid'];
            //printf("Set sysname for name '" . $item['lastvalue'] . "' = " . $item['hostid'] ."\n");
        }
    
        foreach ($conflicts as $sysname => $hostids ) {
          //  error_log("Need to resolve conflict $sysname => ". \json_encode($hostids). "\n");
            unset($sysnames[$sysname]);//the only possible thing is to remove such a hosts
        }
    
        printf("Fetching neighbor info\n");
        $neighbor_items =  self::ApiGetTopoItemsByTag('neighbor');
     //   printf("Got neighbor info:".json_encode($neighbor_items)."\n");
      //  printf("Processing neighbor info, got ".count($neighbor_items)." items\n");
        
        foreach ($neighbor_items as $id => $item) {
     
            
            if ($item['lastvalue'] == "") 
                continue;
            
       //     printf("Processing item: \n".json_encode($item)."\n");
            if (!isset($sysnames[$item['lastvalue']])) { 
        //        printf("Host ". $item['lastvalue']." is not known in sysnames, skipping \n");

                $unknown_hosts[] = [ 'hostid' => $item['hostid'], 
                                     'itemid' => $item['itemid'], 
                                     'name' => $item['lastvalue']
                                   ];
                continue;
            }
    
            $hostid1 = $item['hostid'];
            $hostid2 = $sysnames[$item['lastvalue']];
    
            if (isset($hostid2)) {
                $topo[$hostid1][]=$hostid2;
                $topo[$hostid2][]=$hostid1;
            }
        }
    
        foreach ($topo as $hostid => $hosts) 
                array_unique($topo[$hostid]);

                
        $this->ApiGetTopoRootHosts();

        return [&$topo, &$conflicts, &$unknown_hosts];
    }
    
    private function ApiUpdateTopology() {
        $params = [
            'filter' => [
                "name" => $this->topo_name,
            ],
        ];
        $existing_depends =  self::ApiCall("hostdepends.get", $params);

        printf("Loaded exitsing topologies from the API:".json_encode($existing_depends)."\n");
        printf("Total exitsing topologies from the API:".count($existing_depends)."\n");

        foreach ($existing_depends as $idx => &$dep ) {
            $key = $dep['hostid_up'].' -> '.$dep['hostid_down'];

            if (isset($this->deps[$key])) {
                unset($this->deps[$key]);
                unset($existing_depends[$idx]);
            }
        }

        if (count($existing_depends) > 2000) { //a circuitbreaker to prevent mass delete
            printf("Too much deps to remove:". count($existing_depends)." \n");
            exit;
        }
        printf("Deleting deps:".json_encode($existing_depends)."\n");
        self::ApiDeleteDeps(array_column($existing_depends,'depid'));
        self::ApiCreateDeps();
    }
    private function ApiCreateDeps() {
        
        $options = [
            "name" =>$this->topo_name,
        ];

        printf("Creating ".count($this->deps)." depends\n");

        foreach ($this->deps as $name => $dep) {
            $options['hostid_up'] = $dep['up'];
            $options['hostid_down'] = $dep['down'];
            self::ApiCall('hostdepends.create', $options);
            printf("Inserted dep up=>". $dep['up'].", down=>".$dep['down']."\n");
        }
    }
    private function ApiDeleteDeps($depids) {
        
        $options = [
            'params' => $depids,
        ];

    //    printf("Deleting dependancies:".json_encode($options));
        printf("Deleting ".count($options['params'])." depends\n");
        self::ApiCall('hostdepends.delete', $depids);
    }
    
    private function ApiConnectByToken($url, $token) {
        try {
            $this->api->loginToken($url, $token, ['timeout' => 300]);
            //this is similar to: $result = $zbx->call('apiinfo.version');
            $version = $this->api->getApiVersion();
            print "Remote Zabbix API Version:$version\n";
        } catch (ZabbixApiException $e) {
            print "==== Zabbix API Exception ===\n";
            print 'Errorcode: '.$e->getCode()."\n";
            print 'ErrorMessage: '.$e->getMessage()."\n";
            exit;
        } catch (Exception $e) {
            print "==== Exception ===\n";
            print 'Errorcode: '.$e->getCode()."\n";
            print 'ErrorMessage: '.$e->getMessage()."\n";
            exit;
        }
    }
    
    private function ApiGetTopoItemsByTag($tagvalue) {
        $params = [
            'output' => ['itemid','lastvalue','hostid'],
            'evaltype' => 0,
            "selectTags" => ["tag", "value" ],
            'tags' => [['tag' =>$this->topo_name, 
                        'value' => $tagvalue, 'operator' =>1]],
            'nodebug' => 1
        ];
        return self::ApiCall('item.get', $params);
    }

    private function ApiGetTopoRootHosts() {
        $params = [
            'output' => ['hostid','name'],
            'tags' => [['tag' =>$this->topo_name, 
                        'value' => 'root_host', 'operator' =>1]],
            'inheritedTags' => true,
        ];

        $roots = self::ApiCall('host.get', $params);
        
        foreach ($roots as $ids => $root_host) {
            $this->root_hosts[$root_host['hostid']] = 1;
        }
        
        printf("Got topo roots :\n".json_encode($this->root_hosts)." \n");
    }

    private function ApiCall($method, $options = []) {
        try {
              $result = $this->api->call($method, $options);
            //  print("Retrieved method $method with options ". json_encode($options)."\n total ". count($result)." records \n");
              return $result;    
          } catch (ZabbixApiException $e) {
              print "==== Zabbix API Exception ===\n";
              print 'Errorcode: '.$e->getCode()."\n";
              print 'ErrorMessage: '.$e->getMessage()."\n";
              exit;
          } catch (Exception $e) {
              print "==== Exception ===\n";
              print 'Errorcode: '.$e->getCode()."\n";
              print 'ErrorMessage: '.$e->getMessage()."\n";
              exit;
          }
    }
}