<?php declare(strict_types = 1);
/*
** Zabbix
** Copyright (C) 2001-2022 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Modules\TopologyView\Actions; 
use CControllerResponseData; 
use CControllerResponseFatal; 
use CController as CAction; 
 
class CControllerTopologyRootsList extends CAction {

	private $host;
	private $tag;

	protected function init() {
		$this->disableCsrfValidation();
	}

	protected function checkInput() {
		$fields = [
			'tag' => 'required',
		];

		$ret = $this->validateInput($fields);

		if (!$ret) {
			$this->setResponse(new CControllerResponseFatal());
		}
		
		return $ret;
	}

	protected function checkPermissions() {
		if (!$this->checkAccess(\CRoleHelper::UI_MONITORING_MAPS)) {
			return false;
		}

		return true;
	}

	protected function doAction() {

		if ($this->hasInput('severity_min')) {
			$severity_min = $this->getInput('severity_min');
		}
		
		$this->tag = $this->getInput('tag');
		$response = new CControllerResponseData([
			'tag' => $this->tag,
		]);

		$this->setResponse($response);
	}
}
