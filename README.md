
# Визуализация топологии по данным в Glaber
English version below



## Внимание: 
модуль работает в Glaber начиная с версии 2.15.0. Попытка использовать его в более ранних версиях или в Zabbix будет приводить  к остановке работы всего UI. 
## Еще внимание: 
модуль имеет статус «альфа». Его функционал, настройки, идеи могут значительно меняться. 

## Быстрый старт:

### Сбор данных
1. в каталоге модулей во фронтенде установите модуль топологий:
```
cd modules
git clone https://gitlab.com/mikler/glaber-topology-visualizer/
```
2. Импортируйте шаблон `ospf_topology.yaml`
3. Назначьте шаблон на устройства, работающие с протоколом OSPF

### Построение топологии
1. Сгенериуйте token для доступа в API. Token нужен для построения топологии и обновления связей узлов. Поэтому токен нужно сгенерировать от имени пользователя, имеющего права на чтение и запись необходимых узлов. Токен можно сгененрировать через пользовательский интерфейс в левом меню в подменю Пользователя.
2. Скопируйте файл с настройками 
``` 
cd glaber-topology-visualizer
cp settings.php.example settings.php
```
3. Пропишите полученный токен в настройки, пропишите url, по которому доступно АПИ. Примечание: нужно только имя сервера. 
Пример получившейся конфигурации: 
```
<?php declare(strict_types = 1);

    return [
        'topology_tags' => [
            'topo_ospf' => 'OSPF'
        ],
        'API_KEY' => '954f56..............................9dfbfda5f',
        'API_URL' => 'https://api.glaber.loc'
        ];
 ?>
```
4. Сгенерируйте топологию: 
```
php misc/topo_builder.php
```
Во время генерации по собранным данным обновляются связи между устройствами. Чтобы поддерживать связи актуальными, стоит прописать генерация в cron с необходимой периодичностью, например, раз в 15 минут.

5. Включите модуль топологии в меню Администрирование->Модули
Нажмите "Обновить", должен отобразиться модуль топологии в списке, далее модуль нужно включить

6. В меню мониторинга появится раздел топология, где можно будет посмотреть список устройств, участвующих в топологии, при переходе по имени устройства, будет отрисована топология.
Также ссылка на топологию устройства будет доступна при выдаче результатов поиска

## Общая идея построения топологии: 

Топология позволяет отобразить на карте узлы, связи между ними, состояния узлов и связей, использовать оперативные данные для отображения на карте и изменения своиств объектов.

![Topology](/img/topology.png)

Топология строится, начиная от какого-то элемента сети, рекурсивно, до достижения границ топологии или до достижения максимальной вложенности (**32 хопа**).

Для отображения в списке узлов топологии узлы можно промаркировать тегом начала топологии.

В процессе построения топологии анализируются аномалии и формируются два списка, по которым можно найти хосты с одинаковыми и низвестными именами. Списки можно посмотреть в меню Topology->Topology problems. Эти списки полезны для наведения порядка в ИТ инфраструктуре и конфигурации мониторинга

## Масштабирование 
Топологию можно масштабировать независимо от основного интерфейса. 

Топология загружается в режиме fit to screen. 

При мелком масштабе отображаются только узлы и линки, при увеличении масштаба автоматически отображаются подписи узлов, линков, ширирна линков.

![Small size](/img/topo_tiny.png)

## Множественные топологии

Топологий может быть несколько видов в рамках одного домена мониторинга. 

Технологически разные топологии, или топологии на стыке административного разделения управляющего персонала лучше делить в разные топологии. 

Например, сеть доступа может иметь тег **topology_lldp** для уровня доступа, и **topoplogy_core_lldp** для l2 уровня ядра сети.  

В топологии одного типа может быть несколько несвязных между собой «островов». 

Построение топологии всегда подразумевает указание узла в этой топологии, от которого будет строиться топология до достижения пределов топологического «острова». 

## Режим выделения:

При щелчке на узле выделяется узел и все его связи. 

![NodeSelect](/img/node_select.png)

При выборе класса выделяются все узлы и связи, относящиеся к классу (имеющие данные, полностью совпадающие с классом). 

![NodeSelect](/img/class_select.png)

При поиске выделяются все узлы и линки, имеющие данные и имя узла, частично или полностью совпадающих с введённой строкой поиска. 

![search](/img/mac_search.png)

При первичной загрузке карты выделяется узел, соответствующий хосту, для которого была построена топология.

## Интеграция в систему

Для узлов, у которых назначен или унаследован тег с именем топологии, будет отображаться тег топологии в списке ссылок на оперативные данные узла.
![search](/img/integration_link.png)

# Разметка данных для построения топологии

Для построения топологии необходимо разметить данные с помощью тегов.

1.	Выберите имя тега для вашей топологии, например
```
topology_l2
topology_ospf
topo_mail
```
Имя тега может быть любым, но желательно, чтобы оно указывало на то, что это тег для разметки топологии и имело в себе имя названия предметной части топологии.


Для интеграции в систему выбранные имена тегов вместе с коротким описанием  нужно записать в файле конфигурации модуля: **settings.php**  (Воспользуйтесь примером из **settings.php.example** ):
```
<?php declare(strict_types = 1);
    return [
        'topology_tags' => [
            'topology_lldp' => 'Access topology',
            'topo_ospf' => 'OSPF topology']
        ];
 ?>
```

Для разметки данных в рамках одной и той же топологии нужно использовать одно и то же выбранное имя тега и следующие значения тега: 

### 'sysname'
 тег для привязки объекта хоста в мониторинге к узлу на карте. 
 
 Этим тегом нужно протегировать элемент данных, содержащий имя узла в выбранной топологии. Например, для топологии l2, строящейся по протоколам lldp/cdp и подобным, нужно собирать имя устройства (hostname). Для протоколов маршрутизации – как правило, идентификатор роутера (RouterID). В каждом типе топологии у одной и той-же системы может быть разное имя

### 'neighbor' 
тег для маркировки элемента данных, содержащего имя соседнего узла. 

Это основной тег для построения связи. Имя узла ищется среди метрик, протегированных тегом sysname. 

Предполагается, что тег топологии со значением neighbor привязан к динамически созданной метрике в рамках Discovery ( LLD в терминах Zabbix). В таком случае в связке с соседним узлом будет участвовать компонент целиком и для установленной связи все метрики компонента могут быть источником данных о связи.

### 'link_legend_<postfix>'
тег для метрик компонента. 

Метрики, помеченные этим тегом, будут отображаться на метке линка. На линке можно отобразить основные метрики, характеризующие нагрузку на линк, например входящий и исходящий траффик, нагрузку в запросах в секунду и так далее. 

![Link legend](/img/link_legend.png)

Если одним и тем же значением тега протегировано несколько метрик, то используется одна из метрик. 

Чтобы на легенде отобразить несколько метрик, нужно использовать разные постфиксы: **link_legend_in**,  **link_legend_out**

### 'link_usage'
 значение тега для метрик компонента, для подсчета оперативной пропускной способности линка. 
 
 Единицы относительные. Если тегом протегировано несколько метрик, то они суммируются для одного компонента. 
 В рамках одной карты толщина линков рисуется относительно линка с максимальным значением суммарным значением **link_usage**.

 ![Link legend](/img/link_width.png)

### 'link_info' 
тег для метрик компонента. 
 
 ![LinkContext](/img/link_context_menu.png)

Метрики, протегированные тегом топологии с этим значением, будут отображаться в контекстном меню линка.  

### 'search'
тег может быть использован для любых метрик. 

Данные, протегированные тегом топологии со значение search будут проверяться на вхождение строки поиска, вводимой в поле поиска.  

При совпадении метрики линка полсвечивается линк и хост, которому принадлежит совпадающий элемент данных. 

### 'class_\<postfix\>'
тег аналогичен тегу **search** за исключением того, что вместо строки поиска будет создан элемент выбора значения combobox c именем **postfix** , где будут указаны все различные значения. 

Метрика интерпретируется как список значений, разделенных запятой, поэтому множественные значения можно записать в одну метрику. 

Работает для подсветки хоста или линка. 

Если поиск по классу срабатывает для линка, то подсвечивается и хост, с чьей стороны произошло совпадение. 

Поиск по классу логично использовать для мало-вариативных данных, например, максимальная емкость линка (100G, 10G, 1G), название модели узла, версии ПО и так далее. 

### 'host_info'
Имя и значение метрик, протегированных тегом топологии, будут отображены в контекстном меню хоста при клике правой кнопкой мыши по узлу.  

![LinkContext](/img/host_context_menu.png)

### 'weight'
Вес хоста в топологии. Считается по сумме всех числовых метрик с этим тегом. 

Если в топологии есть хотя-бы один хост с ненулевым тегом weight, то топология будет строиться в форме дерева, начиная от узла с максимальным суммарным значением метрик с тегом топологии и значением weight.
Топология с весами и без:

![topoweight](/img/topo_weight.png)  ![toponoweight](/img/topo_no_weight.png)

## Разметка хостов

### 'root_host'
узлы с таким тегом (своим или наследованным от шаблона) попадут в список корневых узлов в меню топологии. 

Такая функция позволяет строить список узлов для отображения топологий и отображать их в меню топологии. В списке отображается поле из инвентаризации - описание сайта. 

### Ссылка на топологию для хоста в интефейсах
Задайте хосту или шаблону тег топологии с любым значением. По наличию тега топологии (своего или наследованного от шаблона) в контекстных меню поиска у хоста будет отображаться ссылка, которая будет вести к построению топологии начиная от этого хоста

# Topoogy visualisation in Glaber 

## Attention: 
The module is functional only in Glaber 2.15.0 and later. 

## One more note:
The module is still in its alfa stage, so bugs are quite possible as well it might be changed a quite lot.


## The topology visualisation idea:

The intent of the module is combine monitoring data with links and relations gathered automatically and use recent operational data for altering map view and behaviour.

Topology is bult starting from a selected host recursively until reach topology boundaries or reaching maximum recursion level of 32.

Hosts that has to appear in the topology roots list, has to be marked with the '**start of the topology**' tag.

During the topology building and analisys, two list are created: hosts with the same names and names that are misssing from the monitoring.

The lists might be opened via Topology menu and intented to be used to fix configuration of devices and/or monitoring configuration

## Toplogy map zooming and interaction
The topology map is zoomable undependatly from main UI interface.

Upon rendering,  map is autozoomed to fit to screen.

When zoomed out, some details will be hidden.


## Multiple topologies
There might be multiple topologies in the same monitoring domain.

It's advised to split topologies on technical levels or administrative boundaries.

For example, an accsess-level network might be tagged with **topology_lldp** tag and core level network might use  **topoplogy_core_lldp** tag for L2 level topology rendering.  

Topology of the same type might consist of several "islands". 

Build of a topology always assumes that a host, from which topology is built is selected. Topology is built strating from the host intil reaching all the hosts in the topology 'island'.

## Toplogy map highliting
Clickng on a node will lead to highliting the node and all it's links. 

If maps has class data defined, then selecting a class value will highlight all the nodes and edges that have data with same class tag and same value.

Search field will highlight nodes having data or node name fully or partially matching the string entered to the search field.

On initial rendering, the host wich was selected for the topology building is highlighted.

## Map objects colorisation
If there is a trigger that has value 1 (trigger is triggered), then the host is colorised to the trigger's severity. If there are several triggers triggered, then color for the highest severity is used.

If a triggred trigger is calculated on items, which are bound to a link via entity, then the link is also colorised using maximum triggered triggers severity.

## UI integration

For hosts marked with own or inherited topology tag with any value threre will be a link to it's topology generated from the host. 

The link will be shown on all interfaces and context that show links to the host's operational data. 

# Metrics marking for topology

For topolgy, items or metrics must be marked with special topology tags.

Mind a topology tag names, that will represent your topology or topologies. Examples:
```
topology_l2
topology_ospf
topo_mail
```
Topology tag name might be any valid tag name. It's advised for a good UX to have a topology word in the tag name along with topology nature.

Write selected topology names along with short description to the module config file: **settings.php**  (Example is in **settings.php.example** ):
```
<?php declare(strict_types = 1);
    return [
        'topology_tags' => [
            'topology_lldp' => 'Access topology',
            'topology_ospf' => 'OSPF topology']
        ];
 ?>
```
Mark items with the topology tag. 

Use same topology tag name with following predefined names for marking items: 

### 'sysname'

Used for binding a monitoring host to a topology name.
 
Use the tag to mark item that holds the name of the host in the toplogy. The same host might hive different names in different topologies.

For example, in LLDP topology it will be hosts hostname, but for OSPF or BGP will be RouteID in those protocols.

### 'neighbor' 
value for marking items holding neighbor hosts names.

This is a tag value for building an edge between two hosts. 

To build a link the value of the metric should match to a metric marked with sysname value on another host.

It's assumed that the tag is used on dynamic items discovered via Discovery process. 

In such a case all the metrics that where created during discovery of the same host entity will be bound to the link. 

And this is the only way to bound other items and link alternation metrics to the link, so context menus, trigger colorisation, link width and legend will work.

### 'link_legend_\<postfix\>'
tag value is used for entity items

Items, tagged with this value will be shown on as link legend. 

It's typically usefull to show metrics that will represent the link state or link data flow. For example, inbound and outbound traffic. There might be several metrics on the legend, but only one with the same \<postfix\> is dispalyed. 
Use different postfixes to show several metrics, for example: **link_legend_up**,  **link_legend_down**

### 'link_usage'
tag value is used for entity items, to calc link utilisation 
 
Value of the metric is relative and compared to other links utilisation. Utilisation is used for rendering links width. Link with maxumum absulute utilisation is rendered with maximum width, other link are renedered proportionally according to its usage values, but no less them minimum width.


### 'link_info' 
tag value is used for entity items. 

Items marked with the value will be shown along with its names on the link context menu

### 'search'
tag value is used for any metric

Metrics having search tag value will be checked if the search string is a substring of the metric. If so, the object will be highlighted. Link is highlighted with the host the matched data belongs to.


### 'class_\<postfix\>'
tag value is used for any metric and very similar to **search** tag value

For class values, a comboboxe is cretaed per each diffent \<postfix\> value. Such a combobox will hold all unique values of metric for the class tag value.

Class metrics values are interpreted as comma separated values.

Class metrics are intended to use for low-variatinal data for example, link speed, device model, and so on.

### 'host_info'
tag valus is used in any metric

Metrics will be shown on host context menu.

### 'weight'
tag valus is used in any metric

Used for host weight calculatian. Metrics with this tags value are summarised per host.

If there are a host with non-zero weight in the topology, then topology is bult as a tree starting from the host with the maxumum calculated weight value.

## Hosts marking

### 'root_host'
Hosts having or inheriting this tag value  will be listed in the root toplogy list for the topology.

This allow to show a list of the primary hosts per topology island and show them in the topology menu along with their inventory site description field.

### Ссылка на топологию для хоста в интефейсах
Hosts having topology tag with any value will have link to the topology in all the views where host's operational data links are displayed.