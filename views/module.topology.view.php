<?php declare(strict_types = 1);
/*
** Zabbix
** Copyright (C) 2001-2022 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


/**
 * @var CView $this
 */

echo '<script type="text/javascript" src="modules/glaber-topology-visualizer/views/js/g6.min.js"></script>';

$nodes = [];
$links = [];
$options = [];
$topo_type = 'force';

$topology = new \Modules\TopologyView\CTopology($data['hostid'], $data['tag'], 
			$data['onlyrootpath'], $nodes, $links, $hostname, $options);

if (TOPOLOGY_TREE == $topology->getTopologyType())
	$topo_type = 'dagre';


echo ('
<script type="text/javascript">
  const initData = {
    nodes: '. $topology->GetAntvG6NodesMap(). ',
    edges: '. $topology->GetAntvG6EdgesMap(). ',
  };

  G6.Util.processParallelEdges(initData.edges);

  $( document ).ready(function()  {
	
	const container = document.getElementById(\'mynetwork\');
	const width = container.scrollWidth;
	const height = container.scrollHeight || 1000;
	
	var div = document.createElement("div");
	const label = document.createTextNode("Search:");
	var searchfield = document.createElement("input");
	searchfield.id = "search";
	searchfield.value = "'.$hostname.'";

	div.appendChild(label);
	div.appendChild(searchfield);

	container.appendChild(div);
	'.$topology->GetAntvG6Selectors('div').'

	const graph = new G6.Graph({
    	container: "mynetwork", 
    	width: width, 
    	height: height, 
		fitViewPadding: 20,
		fitView : true,
		linkCenter : true,
		modes: {
			default: [ 
					{"type":"drag-canvas", },
					{"type":"zoom-canvas",
						"enableOptimize":true,
						"optimizeZoom":0.2,

					},
					{"type":\'drag-node\'},
					{"type":"activate-relations", 
						"trigger": "click" },
				], 
		},

		layout: {
			type: "'.$topo_type.'",
			rankdir: "TB",
			nodesep: 20,
			ranksep: 200,
		    nodeSpacing: 150,
			linkDistance: 350,
			nodeStrength: -600,
			collideStrength: 0.1,
			nodeSize: 70,
			alpha: 0.3,
    		alphaDecay: 0.028,
    		alphaMin: 0.05,
			
		 },

		defaultNode: {
		  type: "circle",
		  size: 45,
		  stroke: "#adc6ff",
		  lineWidth: 6,
		  labelCfg: {
		  
		  	style: {
				fill: "#3B6FE9",
				fontSize: 14,
				background: {
					fill: "#ffffff",
					stroke: "#9EC9FF",
					
					padding: [2, 2, 2, 2],
					radius: 2,
			  	},
			},
			position: "bottom",
			offset: 10,
		  },
		  
		  style: {
			   stroke: \'#3B6FE9\',
			   lineWidth: 8,
			 },
		},	
		nodeStateStyles: {
			inactive: {
				lineWidth: 8,
				stroke: \'#b6c7cf\',
			},
			"active": {
				lineWidth: 4,
				stroke: \'#007ab3\',
				fill: "red",
			},
		},

		edgeStateStyles: {
			inactive: {
				stroke: \'#b6c7cf\',
				lineWidth: 4,
			},
			active: {
				lineWidth: 8,
				stroke: "red",
				//stroke: \'#007ab3\',
			},
		},
		defaultEdge: {
			labelCfg: {
				autoRotate: true,
				style: {
					lineWidth: 1,
					fill: "#1890ff",
					fontSize: 10,
					stroke: "#6B9FF9",
					background: {
						fill: "#ffffff",
						stroke: "#9EC9FF",
						padding: [2, 2, 2, 2],
						radius: 2,
			  		},
				},
			},
			type: "line",
			style: {
			   stroke: \'#5B8FF9\',
			   lineWidth: 1,
			},
		},
	});
	
	graph.on(\'node:mouseenter\', (e) => {
		const nodeItem = e.item; // Get the target item
		graph.setItemState(nodeItem, \'hover\', true); // Set the state \'hover\' of the item to be true
	});
	  
	 
	graph.on(\'node:mouseleave\', (e) => {
		const nodeItem = e.item; // Get the target item
		graph.setItemState(nodeItem, \'hover\', false); // Set the state \'hover\' of the item to be false
	});

	graph.on(\'node:contextmenu\', (e) => {
		const nodeItem = e.item; // Get the target item
		var graph_div = document.getElementById(\'mynetwork\');//.getElementsByTagName(\'canvas\')[0];
		var newdiv= document.createElement("div");
		newdiv.setAttribute(\'data-menu-popup\', "{\"type\":\"host\", \"data\":{\"hostid\":" + nodeItem._cfg.id + "}}");
		newdiv.style="position:absolute; border: 1px; left:" + Math.round(e.canvasX) +"px; top: " + Math.round(e.canvasY) + "px;";
		graph_div.appendChild(newdiv);
		newdiv.click();
		e.preventDefault();
	});

	graph.data(initData);
    graph.render(); // Render the graph  
	
	const sel_nodes = graph.findAll("node", (node) => {
		return node.get("model").label.includes(searchfield.value);
	});	

	sel_nodes.forEach(node => {
		graph.setItemState(node, "active", true);
	});

	searchfield.addEventListener("input", (e) => {
		const value = e.target.value;
		
		graph.getNodes().forEach(node => {
			graph.setItemState(node, "active", false);
		});
	
		graph.getEdges().forEach(edge => {
			graph.setItemState(edge, "active", false);
		});
	
	
		if (value == "") 
			return;

		const nodes = graph.findAll("node", (node) => {
			return node.get("model").search.includes(value);
		});	
		
		const edges = graph.findAll("edge", (edge) => {
			return edge.get("model").search.includes(value);
		});	

		nodes.forEach(node => {
			graph.setItemState(node, "active", true);
		});
		
		edges.forEach(edge => {
			graph.setItemState(edge, "active", true);
		});
		
	});
});

</script>
');

$web_layout_mode = $this->getLayoutMode();

(new CHtmlPage())    
	->setTitle(_('Topology'))
	->setWebLayoutMode($web_layout_mode)
	->setControls(new CList([""]))
	->addItem(
		(new CDiv())
			->addClass(ZBX_STYLE_TABLE_FORMS_CONTAINER)
			->addStyle('padding: 0;') 
			->addStyle('width: 100%;')
			->setId('mynetwork')
	)
	->show();
