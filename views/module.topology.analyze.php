<?php declare(strict_types = 1);
/*
** Zabbix
** Copyright (C) 2001-2022 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


/**
 * @var CView $this
 */
$web_layout_mode = $this->getLayoutMode();

$same_sysname_table = createSameSysnameTable($data['conflicts']);
$miss_sysname_table = createMissingSysnameTable($data['unknown_hosts'], $data['tag']);

(new CHtmlPage())    
	->setTitle(_('Topology anomalies for type "'.$data['tag']).'"')
	->setWebLayoutMode($web_layout_mode)
	->setControls(new CList([
		(new CForm('get'))
			->cleanItems()
			->setName('topology.view')
			->addVar('action', 'topology.view')
			->setAttribute('aria-label', _('Main filter'))
			->addItem((new CList())
				->addItem([
				])
			),
	]))
		
	->addItem( (new CTag("h4", true, "Conflicts"))
				->addStyle("font-weight: bold;"))
	->addItem($same_sysname_table)
	->addItem( (new CTag("h4", true, "Unknown topo names"))
				->addStyle("font-weight: bold;"))
	->addItem($miss_sysname_table)
	->show();


function &fetchAllHostids(array &$sysname_data) {
	$hostsids = [];
	foreach ($sysname_data as $sysname => $ids) 
		$hostsids = array_merge($hostsids, $ids);	
	
	array_unique($hostsids);
	return $hostsids;
}

function getHostsDataById(array &$hostids) {
	$hosts = \API::Host()->get([
		'hostids' => $hostids,
		"output" => ['hostid','host'],
	//	'selectGroups' => API_OUTPUT_EXTEND,
		'filter' => [
			'status' => [HOST_STATUS_MONITORED, HOST_STATUS_NOT_MONITORED]
		]]);

	$ret = [];
	foreach ($hosts as $host) 
		$ret[$host['hostid']] = $host;
	
	return $ret;
}


function createSameSysnameTable(array &$sysname_data) {
	
	if (count($sysname_data) == 0) {
		return (new CDiv("No duplicate sysnames has been found - OK!"));
	}

	$hostsids = fetchAllHostids($sysname_data);
	$hostsbyid = getHostsDataById($hostsids);

	$table = (new CDataTable('dup_sysnames'))
		->setHeader([
			(new CColHeader(_('Sysname')))->addClass('search'),
			(new CColHeader(_('Hosts')))->addClass('search'),
	]);

	foreach ($sysname_data as $sysname => $hostids) {	
		$row = [];
		$row[] = (new CCol($sysname));

		$host_cell =(new CDiv()); 
		
		$i=0;
		foreach ($hostids as $hostid) {
			if ($i++ > 0)
			$host_cell->addItem(', ');
	
			$host_cell->addItem( (new CLink($hostsbyid[$hostid]['host'],
				(new CUrl('hosts.php'))
					->setArgument('form', 'update')
					->setArgument('hostid', $hostid)))
				->setTarget('_blank') );
				
		}

		$row[] = (new CCol($host_cell));
		$table->addRow($row);
	}

	return $table;
}

function  fetchAllHostAndItemIds(array &$missing_sysnames) {
	$hostids=[];
	$itemids=[];
	foreach ($missing_sysnames as $cnt => $record ) {
		$hostids[] = $record['hostid'];
		$itemids[] = $record['itemid'];
	}

	array_unique($hostids);
	
	return [$hostids, $itemids];
}

function saveEntitiesByItemid(array &$entites) {
	
}

function createMissingSysnameTable(array &$missing_sysnames, $tag) {
//	$temp = null;

//	$items = \Modules\TopologyView\CTopologyItems::fetchItemsAndEntities($temp, $hosts, $data['tag']);

	if (count($missing_sysnames) == 0) {
		return (new CDiv("No missing topology names has been found - OK!"));
	}
	
	$table = (new CDataTable('misssysnames'))
		->setHeader([(new CColHeader(_('Source')))->addClass('search'),
				      (new CColHeader(_('Name')))->addClass('search'),
	]);

	[$hostids, $itemids] = fetchAllHostAndItemIds($missing_sysnames);
	$hostsbyid = getHostsDataById($hostids);

	$items = [];
	$entites = [];

	[$entities, $items] = \Modules\TopologyView\CTopologyItems::fetchItemsAndEntities($hostids, $itemids, []);
	
	//$itemid_to_entity = saveEntitiesByItemid($entites);

	foreach ($entities as $entityid => $entity ) {
		if (!isset($entity['entities']) || !is_array($entity['entities']))
			continue;

		foreach ($entity['entities'] as $ent_name => $ent_items) {
			foreach ($ent_items as $proto_itemid => $itemid) {
			   $itemid_to_entity[$itemid] = $ent_name;
			}
		}
	}
	
	//error_log(json_encode($entities));
	
	foreach ($missing_sysnames as $cnt => $record ) {	
		$row = [];

		$row[] = (new CCol())
				->addItem((new CLink($hostsbyid[$record['hostid']]['host'],
					(new CUrl('hosts.php'))
						->setArgument('form', 'update')
						->setArgument('hostid', $record['hostid'])))
						->setTarget('_blank') )
				->addItem(':')
				->addItem($itemid_to_entity[$record['itemid']]);

		
		$row[] = (new CCol((new CLink($record['name'],
			(new CUrl('zabbix.php'))
				->setArgument('action', 'search')
				->setArgument('search', $record['name'])))
				->setTarget('_blank') 
			));
				
		$table->addRow($row);
	}

	return $table;
}