<?php declare(strict_types = 1);
/*
** Zabbix
** Copyright (C) 2001-2022 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


/**
 * @var CView $this
 */

$options = [];

//$topology = new \Modules\TopologyView\CTopology($data['hostid'], $data['tag'], $nodes, $links, $hostname, $options);
//require_once '../CTopologyRootsList.php';

$topology_hosts = \Modules\TopologyView\CTopologyRootsList::getRootHosts([$data['tag']]);

//show_error_message("Got ".count($topology_hosts). "hosts :". json_encode($topology_hosts) );

$topo_table = createTopoHostsTable($topology_hosts, $data['tag']);

$web_layout_mode = $this->getLayoutMode();

(new CHtmlPage())
	->setTitle(_('List of topology root hosts for topology "'.$data['tag'].'"'))
	->setWebLayoutMode($web_layout_mode)
	->setControls(new CList([
		(new CForm('get'))
			->cleanItems()
			->setName('topology.view')
			->addVar('action', 'topology.view')
			->setAttribute('aria-label', _('Main filter'))
			->addItem((new CList())
				->addItem([

				])
			),
	]))
	->addItem(
		(new CDiv())
			->addClass(ZBX_STYLE_TABLE_FORMS_CONTAINER)
			->addStyle('padding: 0;') 
			->addStyle('width: 100%;')
			->setId('mynetwork')
			->addItem($topo_table)
	)
	->show();

function create_map_screen($map_data) {
	return $map_data;
}

function createTopoHostsTable(array &$topo_hosts, $tag_name) {
	//show_error_message("Gotttt ".count($topo_hosts). "hosts :". json_encode($topo_hosts) );

	$topo_hosts_table = (new CDataTable('topoview'))
		->setHeader([
			(new CColHeader(_('Hostname')))->addClass('search'),
			(new CColHeader(_('Description')))->addClass('search'),
		]);

	
	foreach ($topo_hosts as $host) {

			$row = [];
			$row[] = (new CCol(
				new CLink($host['host'],
					(new CUrl('zabbix.php'))
						->setArgument('action', 'topology.view')
						->setArgument('hostid', $host['hostid'])
						->setArgument('tag', $tag_name)
			)));
				
				
			$site_descr = '';
			if (isset ($host['inventory']) && isset($host['inventory']['site_notes'])) 
				$site_descr = $host['inventory']['site_notes'];
			
			$row[] = (new CCol($site_descr));
			$topo_hosts_table->addRow($row);
		
	}

	return $topo_hosts_table;
}
