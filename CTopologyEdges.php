<?php declare(strict_types = 1);
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Modules\TopologyView;
require_once 'CTopologyUtils.php';

class CTopologyEdge {    
    private $from_id;
    private $to_id;

    private $to_item_ids =[];
    private $from_item_ids =[];
    private $item_ids_count = 0;
    
    private $topoEntities;

    public function __construct($from_host_id, $to_host_id, array $items,
             CTopologyItems $topoItems, CTopologyEntities $topoEntities, CTopologyHosts $topoHosts) {
        $this->from_host_id = $from_host_id;
        $this->to_host_id = $to_host_id;
        $this->from_itemids = $items;

        $this->from_item_ids = $items['from_ids'];
        $this->to_item_ids = $items['to_ids'];

        $this->item_ids_count = max(count($this->from_item_ids),count($this->to_item_ids));
        
        $this->entities =[];
        $this->topoEntities = $topoEntities;
        $this->topoItems = $topoItems;
        $this->topoHosts = $topoHosts;

        for ($i = 0; $i < $this->item_ids_count; $i++) {
            if (!isset($this->from_item_ids[$i]))
                $this->from_item_ids[$i] = 0;
    
            if (!isset($this->to_item_ids[$i]))
                $this->to_item_ids[$i] = 0;    
        }

        foreach ($this->from_item_ids as $i => $itemid) {
            $this->entities[$i] = ["from" => $topoItems->getItemEntity($itemid), 
                                   "to"=> $topoItems->getItemEntity($this->to_item_ids[$i])];

            $this->entity_items[$i] = ["from" => $topoItems->getEntityItems($from_host_id, $this->entities[$i]['from']),
                                       "to" => $topoItems->getEntityItems($to_host_id, $this->entities[$i]['to'])];
        }
    }

    private function getEntities($i) {
        return ($this->entities[$i]);
    }

    private function getAllItemsIds($i) {
        return array_merge($this->entity_items[$i]['from'],$this->entity_items[$i]['to']);
    }

    private function getAllTriggers($i) {
        return $this->topoEntities->getTriggers($this->from_host_id, $this->entities[$i]['from']) + 
               $this->topoEntities->getTriggers($this->to_host_id, $this->entities[$i]['to']); 
        
    }
    
    private function getLinkSearchText($i, $init_text) {
        $search_text = $init_text;

        $link_items_ids = $this->getAllItemsIds($i);
        $link_items = $this->topoItems->getItemsByIds($link_items_ids);
        return $this->topoItems->getItemsSearchText($link_items,'');
    }

    private function getLinkClasses($i) {
        $link_classes = [];

        $link_items_ids = $this->getAllItemsIds($i);

        foreach ($link_items_ids as $itemid) {
            $item = $this->topoItems->getItemById($itemid);
            $class_name = '';

            if ($this->topoItems->isClassItem($item, $class_name)) {
                
                $formatted_value = $this->topoItems->getFormattedValue($item);

                if (!isset($link_classes[$class_name]))
                     $link_classes[$class_name] =[];

                $link_classes[$class_name] = preg_split('/,/',$formatted_value);
            }
        }
        
        return $link_classes;
    }

    private function getLegend($i) {
        $set_suffix=[];
        $legend = "";
        
        $link_items_ids = $this->getAllItemsIds($i);
        
        foreach ($link_items_ids as $itemid) {
            $item = $this->topoItems->getItemById($itemid);
            $legend_name = '';

            if ( !$this->topoItems->isLinkLegendItem($item, $legend_name)) 
                continue;
            
            if (isset($set_suffix[$legend_name]))
                continue;
        
            $set_suffix[$legend_name] = 1;

            if (strlen($legend) > 0) 
                $legend .= ", ";

            $legend .= $this->topoItems->getFormattedValue($item);
        }
        return $legend;        
    }

    private function getSeverity($i) {
        return max($this->topoEntities->getSeverity($this->from_host_id, $this->entities[$i]["from"]), 
                   $this->topoEntities->getSeverity($this->to_host_id,  $this->entities[$i]["to"]));
    }

    private function getWidth($i) {
           
        $max_width = $this->topoItems->getMaxEntityData();
        $entities = $this->getEntities($i);
        
        $entity_data = max( $this->topoItems->getEntityData($this->from_host_id, $entities['from']), 
                            $this->topoItems->getEntityData($this->to_host_id, $entities['to']) );

        if (0 == $max_width)
            return DEFAULT_WIDTH;
            
        return  (int) (MIN_WIDTH + ((MAX_WIDTH - MIN_WIDTH) * $entity_data)/$max_width );
    }

    public function getItemsCount() {
        return $this->item_ids_count;
    }
    

    
    private function makeItemData($i, $direction) {
        $ret = '';
        foreach ($this->entity_items[$i][$direction] as $itemid) {
            $item=$this->topoItems->getItemById($itemid);

            if (!$this->topoItems->isLinkInfoItem($item))
                continue;
     
            $ret .= '<b>'.$item['name'].'</b> : '.CTopologyUtils::escape_json($this->topoItems->getFormattedValue($item)).'</br>';
        }
        return $ret;
    }

    public function getEdgesJSData(){
        $ret = "";

        for ($i = 0; $i < $this->item_ids_count; $i++) {
            if ($i>0)
                $ret .=',';

            $legend = $this->getLegend($i);
            $max_severity = $this->getSeverity($i);
            $color = '';
                          
            if ( $max_severity > 0) 
                $color = '"stroke": "#'. \CSeverityHelper::getColor(intval($max_severity)).'",'; 
            
            $width = $this->getWidth($i);
            
            $ret .= '{"source":"'. $this->from_host_id. '", "target":"'. $this->to_host_id.'",
                     "style": {'.$color.' "lineWidth": "'.$width.'", },
                     "label":"'.$legend.'",
                     "items":'.json_encode($this->getAllItemsIds($i)).',
                     "host1":"'. CTopologyUtils::escape_json($this->topoHosts->getName($this->from_host_id)).'",
                     "entity1":"'. CTopologyUtils::escape_json($this->entities[$i]['from']).'",
                     "host2":"'. CTopologyUtils::escape_json($this->topoHosts->getName($this->to_host_id)).'",
                     "entity2":"'. CTopologyUtils::escape_json($this->entities[$i]['to']).'",
                     "itemdata1":"'.$this->makeItemData($i,'from').'",
                     "itemdata2":"'.$this->makeItemData($i,'to').'",
                     "triggers":'.json_encode($this->getAllTriggers($i)).',
                     "classes":'.json_encode($this->getLinkClasses($i)).',
                     "search":"'.CTopologyUtils::escape_json($this->getLinkSearchText($i,"")).'"}';
        }
        return $ret;
    }
}


class CTopologyEdges {
    private $max_edge_data = 0;
    private $edges=[];
    private $merged_links = [];
    private $rev_links = [];
    private $fwd_links = [];

    private $logger;

    function __construct(&$logger) {
        $this->edges =[];
        $this->logger = $logger;
    }

    
     public function getMaxData() {
        return $this->max_edge_data;
    }

    public function saveLinks($from_id, $to_id, array $items) {
        $this->fwd_links[$from_id][$to_id]=$items;
    }
    
    public function hasBackLink($from_id, $to_id) {
        if (isset($this->rev_links[$from_id]) && 
            isset($this->rev_links[$from_id][$to_id])) {
                return true;
        }
        return false;
    }

    public function saveBackLinks($from_id, $to_id, array $items) {
        $this->rev_links[$from_id][$to_id]=$items;
    }

    public function mergeLinksData() {
        foreach($this->fwd_links as $from_id => $link_data) {
            foreach ($link_data as $to_id => $itemids) {
            
                if (isset($this->rev_links[$to_id][$from_id]))
                    $rev_itemids = $this->rev_links[$to_id][$from_id];
                else 
                    $rev_itemids=[];

                unset($this->rev_links[$to_id][$from_id]);
                //todo: link matching should be done here
                $this->merged_links[$from_id][$to_id]=["from_ids"=> $itemids, "to_ids" => $rev_itemids];
       //         if ($from_id == 15069)
         //           $this->logger->log("Saved link daat for $from_id -> $to_id:".json_encode($this->merged_links[$from_id][$to_id]));
            }
        }
        
        if (count($this->rev_links) > 0) {
            foreach($this->rev_links as $from_id => $link_data) {
                foreach ($link_data as $to_id => $items) {
                    $this->merged_links[$to_id][$from_id]=["to_ids" => $items];
                    
                    if (!isset($this->merged_links[$to_id][$from_id]["to_ids"]))
                        $this->merged_links[$to_id][$from_id]["to_ids"] = [];
                }
            }
        }
    }

    public function dumpLinksData() {
        return \json_encode($this->merged_links);
    }

    public function &getLinksData() {
        return $this->merged_links;
    }

}
