<?php declare(strict_types = 1);
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Modules\TopologyView;
//require_once 'CTopologyCache.php';

define('MAX_HOST_ID', 2294967295);

class CTopologyHosts {
    private $host_severitites = [];
    private $hosts_by_id = [];
    private $hostids_by_name = [];
    private $host_classes = [];
    
    private $max_weight = -1;
    private $max_weight_hostid = 0;
    private $topo = [];
    private $topology_tag;
    private $items;
    private $logger;

    public function __construct(&$logger, $topology_tag) {
        $this->topology_tag = $topology_tag;
        $this->logger = $logger;
    }

    public function setItems(&$items) {
        $this->items = $items;
    }    

    private $last_unknown_hostid = MAX_HOST_ID + 1;

    public function setHostSeverity($hostid, $severity) {
        
        if (!isset($this->host_severitites[$hostid]) ||
            $this->host_severitites[$hostid] < $severity ) 
        {
            $this->host_severitites[$hostid] = $severity;
        }
    }

    public function getHostSeverity($hostid) {
        
        if (isset($this->host_severitites[$hostid]))
            return intval($this->host_severitites[$hostid]);
            return 0;
    }

    private function findHostIdByTopoId($topo_id) {
        if (isset($this->hostids_by_name[$topo_id])) {
            return $this->hostids_by_name[$topo_id];
        }
        
        if (ALLOW_SUBSTRING_HOST_SEARCH)
            return $this->resolveUnknownNameHost($topo_id);
    }
    
    private function resolveUnknownNameHost($topo_id) {
        $matches = 0;
        $matched_hostid = 0;

        foreach ($this->hostids_by_name as $hostname => $hostid) {
            if (strpos($hostname, $topo_id) !== false ) {

                $matched_hostid = $hostid;
                $matches++;

                if ($matches > 1 )
                    break;
            }          
        }

        if (1 == $matches) {
                return $matched_hostid;
        } 
    }

    public function saveSysname($name, $hostid) {
        $this->hostids_by_name[$name] = $hostid;
    }

    public function saveHostsDetails(array &$hosts) {
        foreach ($hosts as $host) {
            $this->hosts_by_id[$host['hostid']] = $host;
            $this->hostids_by_name[$host['host']] = $host['hostid'];
        }
    }

    public function createUnknownHost($hostname) {
        $hostid =  $this->last_unknown_hostid++;
        
        $hosts[] = ['hostid' => $hostid,
                    'host' => $hostname ];
                 
        $this->saveHostsDetails($hosts);
        
       // error_log("Creating unknown host $hostname -> $hostid\n");
        
        return $hostid;
    }

    public function getById($hostid) {
        if (isset($this->hosts_by_id[$hostid])) 
            return $this->hosts_by_id[$hostid];
        return [];
    }

    public function getName($hostid) {
        if (isset($this->hosts_by_id[$hostid])) {
            return $this->hosts_by_id[$hostid]['host'];
        }
        return NULL;
    }

    public function isUnknownHostid($hostid) {
        if ($hostid > MAX_HOST_ID) 
            return true;
        return false;
    }

    public function setHostClasses($hostid, $classname, array &$host_classes) {
        if (count($host_classes) == 0)
            return;

        if (!isset($this->host_classes[$classname]))
            $this->host_classes[$classname] = [];
        
        foreach ($host_classes as $host_class) {
            if (!isset($this->host_classes[$classname][$host_class])) 
                $this->host_classes[$classname][$host_class] = [];
            
            $this->host_classes[$classname][$host_class][$hostid] = 1; 
        }
    }   

    public function dumpHostAllClasses() {
        return \json_encode($this->host_classes);
    }
    public function dumpHostClasses($classname) {
        $ret=[];
        if (isset($this->host_classes[$classname])) {
            foreach ($this->host_classes[$classname] as $class => $hosts) {
                $ret[$class] = array_keys($hosts);
            }
        }
        return \json_encode($ret);
    }
    
    public function getClassesNames() {
        return array_keys($this->host_classes);
    }

    public function getClasses($classname) {
        if (isset($this->host_classes[$classname]))
            return array_keys($this->host_classes[$classname]);
        return[];
    }

    public function getClassHosts($classname, $class) {
        if (isset($this->host_classes[$classname][$class]))
            return array_keys($this->host_classes[$classname][$class]);
        return [];
    }

    public function setMaxWeightHost($hostid, $weight) {
        if ($this->max_weight < $weight ) {
            $this->max_weight_hostid = $hostid;
            $this->max_weight = $weight;
        }
    }

    public function getMaxWeightHostid() {
        return $this->max_weight_hostid;
    }

    public function getHostsInterlinks($hostid1, $hostid2) {
        $ret =[];
               
        $ret['from'] = $this->filterNeighborHostItems($hostid1, $hostid2);
        $ret['to']   = $this->filterNeighborHostItems($hostid2, $hostid1);
        
        return $ret;
    }   

    private function filterNeighborHostItems($hostid1, $hostid2) {
        $ret = [];
        foreach( $this->items->getItemsByHostId($hostid1) as $item) {
            if (! $this->items->isNeighborItem($item)) 
                continue;
            
            $neighbor_host_id = $this->findHostIdByTopoId($item['lastvalue']);
            
            if (!isset($neighbor_host_id)) {
                $neighbor_host_id = $this->createUnknownHost($item['lastvalue']);
                $this->topo[$hostid1][] = $neighbor_host_id;
            }

            if ($neighbor_host_id == $hostid2)
                $ret[] = $item['itemid'];
        }
        
        return $ret;
    }

    private function saveHosts(array &$hosts) {
        $this->saveHostsDetails($hosts);
        $hostids = array_column($hosts, 'hostid');
        $this->items->fetchAndSaveItems($hostids, [], [ [ 'tag' => $this->topology_tag, 'operator' => 4 ]]);
    }

    private function saveHostsOnly(array &$hosts) {
        $this->saveHostsDetails($hosts);
//        $hostids = array_column($hosts, 'hostid');
    }


    public function fetchTopologyDataFromAPI(array $hostids) {
    //    error_log("Got hostids: ". json_encode($hostids));
        \show_error_message("Fetching ". count($hostids). " hosts");
        
        $start = floor(microtime(true) * 1000);
        
        $hosts = \API::Host()->get([
            'hostids' => $hostids,
            "output" => ['hostid','host'],
            'nodebug' => 1,
            'filter' => [
                'status' => [HOST_STATUS_MONITORED, HOST_STATUS_NOT_MONITORED]
            ]]);
     //   \show_error_message("fetch hosts from api " .(floor(microtime(true) * 1000) - $start) ." ms");

        
        $this->logger->log("Fetched ". count($hosts)." hosts");
        
        $start = floor(microtime(true) * 1000);
        $this->saveHosts($hosts);
     //   \show_error_message("save hosts api " .(floor(microtime(true) * 1000) - $start) ." ms");

    }
    
    public function fetchHostsDataFromAPI(array $hostids) {
        //    error_log("Got hostids: ". json_encode($hostids));
        //    \show_error_message("Fetching ". json_encode($hostids). " hosts");
            
            $hosts = \API::Host()->get([
                'hostids' => $hostids,
                "output" => ['hostid','host'],
                'nodebug' => 1,
                'filter' => [
                    'status' => [HOST_STATUS_MONITORED, HOST_STATUS_NOT_MONITORED]
                ]]);
            
            $this->logger->log("Fetched ". count($hosts)." hosts");
            
            //$start = floor(microtime(true) * 1000);
            $this->saveHostsOnly($hosts);
          //  \show_error_message("save hosts api " .(floor(microtime(true) * 1000) - $start) ." ms, saved ".count($hosts). " hosts");
        }

}