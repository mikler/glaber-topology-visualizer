<?php 
/*
** Glaber
** Copyright (C) Glaber 
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/


declare(strict_types = 1);
namespace Modules\TopologyView;

error_reporting(-1);
ini_set('display_errors', 'On');
define("ALLOW_SUBSTRING_HOST_SEARCH", false);

class CTopologyRootsList{
    
    static public function &getRootHosts(array $topo_tags) {
        $tags = [];
        foreach ($topo_tags as $topo_tag) {
            $tags[] =  [ "tag" => $topo_tag,
                "value" => "root_host",  
                "operator" => 1             
            ];
        }
        
        $hosts =\API::Host()->get([
            "output" => ['hostid','host','name'],
            'selectInventory' => ['site_notes'],
            "selectTags" => "extend",
            "selectInheritedTags" => "extend",
            "evaltype" =>  2, 
            "tags" => $tags,
            'inheritedTags' => true,
            'filter' => [
                'status' => [HOST_STATUS_MONITORED, HOST_STATUS_NOT_MONITORED]
            ]]);
        
        return $hosts;
    }
}